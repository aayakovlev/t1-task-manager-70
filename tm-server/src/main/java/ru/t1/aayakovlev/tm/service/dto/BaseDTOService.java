package ru.t1.aayakovlev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractModelDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface BaseDTOService<E extends AbstractModelDTO> {

    @NotNull
    E save(@Nullable final E model) throws AbstractException;

    @NotNull
    Collection<E> add(@Nullable final Collection<E> models) throws AbstractException;

    void clear() throws AbstractException;

    long count() throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<E> findAll() throws AbstractException;

    @NotNull
    List<E> findAll(@Nullable final Comparator<E> comparator) throws AbstractException;

    @NotNull
    E findById(@Nullable final String id) throws AbstractException;

    void remove(@Nullable final E model) throws AbstractException;

    void removeById(@Nullable final String id) throws AbstractException;

    @NotNull
    Collection<E> set(@Nullable final Collection<E> models) throws AbstractException;

    @NotNull
    E update(@Nullable final E model) throws AbstractException;

}
