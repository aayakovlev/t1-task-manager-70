//package ru.t1.aayakovlev.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//@Category(UnitCategory.class)
//public final class PropertyServiceImplTest {
//
//    @NotNull
//    private final PropertyService service = new PropertyServiceImpl();
//
//    @Test
//    public void When_GetApplicationVersion_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getApplicationVersion());
//    }
//
//    @Test
//    public void When_GetApplicationConfig_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getApplicationConfig());
//    }
//
//    @Test
//    public void When_GetAuthorName_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getAuthorName());
//    }
//
//    @Test
//    public void When_GetAuthorEmail_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getAuthorEmail());
//    }
//
//    @Test
//    public void When_GetGitBranch_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitBranch());
//    }
//
//    @Test
//    public void When_GetGitCommitId_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitCommitId());
//    }
//
//    @Test
//    public void When_GetGitCommitterName_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitCommitterName());
//    }
//
//    @Test
//    public void When_GetGitCommitterEmail_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitCommitterEmail());
//    }
//
//    @Test
//    public void When_GetGitCommitMessage_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitCommitMessage());
//    }
//
//    @Test
//    public void When_GetGitCommitTime_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getGitCommitTime());
//    }
//
//    @Test
//    public void When_GetPasswordIteration_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getPasswordIteration());
//    }
//
//    @Test
//    public void When_GetPasswordSecret_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getPasswordSecret());
//    }
//
//    @Test
//    public void When_GetHost_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getHost());
//    }
//
//    @Test
//    public void When_GetPort_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getPort());
//    }
//
//    @Test
//    public void When_GetSessionKey_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getSessionKey());
//    }
//
//    @Test
//    public void When_GetSessionTimeout_Expect_ReturnNotNull() {
//        Assert.assertNotNull(service.getSessionTimeout());
//    }
//
//}
