package constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

public final class ProjectTestConstant {

    @NotNull
    public static ProjectDTO PROJECT_ONE = new ProjectDTO("First");

    @NotNull
    public static ProjectDTO PROJECT_TWO = new ProjectDTO("Second");

    @NotNull
    public static ProjectDTO PROJECT_THREE = new ProjectDTO("Third");

    @NotNull
    public static ProjectDTO PROJECT_FOUR = new ProjectDTO("Fourth");

}
