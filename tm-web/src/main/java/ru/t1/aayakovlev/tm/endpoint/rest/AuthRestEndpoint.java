package ru.t1.aayakovlev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.rest.IAuthRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IUserDTOService;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/auth")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @PostMapping(value = "/login")
    public boolean login(@NotNull final String username, @NotNull final String password) {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication.isAuthenticated();
    }

    @GetMapping("/logout")
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_USER"})
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @NotNull
    @GetMapping("/profile")
    @Secured({"ROLE_ADMINISTRATOR", "ROLE_USER"})
    public UserDTO profile() {
        @NotNull final SecurityContext context = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = context.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userDTOService.findByLogin(login);
    }

}
