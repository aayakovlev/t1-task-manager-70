package ru.t1.aayakovlev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;

import java.util.List;

public interface ITaskDTOService {

    long countByUserId(@Nullable final String userId) throws UserIdEmptyException;

    @Transactional
    void deleteAllByUserId(@Nullable final String userId) throws UserIdEmptyException;

    @Transactional
    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws AbstractException;

    @NotNull
    TaskDTO findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    @Transactional
    TaskDTO save(@Nullable final TaskDTO task) throws EntityEmptyException;

}
