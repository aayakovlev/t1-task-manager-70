package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.User;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull final String login);

}
