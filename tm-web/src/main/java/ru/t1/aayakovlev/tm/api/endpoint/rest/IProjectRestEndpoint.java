package ru.t1.aayakovlev.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    void deleteAll() throws AbstractException;

    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    List<ProjectDTO> findAll() throws AbstractException;

    @NotNull
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    ProjectDTO findById(@NotNull final String id) throws AbstractException;

    @NotNull
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    ProjectDTO save(@NotNull final ProjectDTO project) throws EntityEmptyException;

    @NotNull
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    ProjectDTO update(@NotNull final ProjectDTO project) throws EntityEmptyException;

}
