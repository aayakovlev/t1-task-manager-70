package ru.t1.aayakovlev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

public interface IUserDTOService {

    long count() throws AbstractException;

    @Transactional
    void deleteAll();

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<UserDTO> findAll() throws AbstractException;

    @NotNull
    UserDTO findById(@Nullable final String id) throws AbstractException;

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @NotNull
    @Transactional
    UserDTO save(@Nullable final UserDTO user) throws EntityEmptyException;

}
