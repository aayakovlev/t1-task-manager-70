package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.aayakovlev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task", schema = "public", catalog = "task_manager")
public final class Task {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "name", columnDefinition = "VARCHAR(64)")
    private String name;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "created", columnDefinition = "TIMESTAMP")
    private Date created = new Date();

    @NotNull
    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "VARCHAR(64)")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
