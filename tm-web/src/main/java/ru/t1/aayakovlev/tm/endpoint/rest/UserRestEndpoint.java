package ru.t1.aayakovlev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.rest.IUserRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IUserDTOService;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users")
public class UserRestEndpoint implements IUserRestEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService service;

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    public void deleteAll() throws AbstractException {
        service.deleteAll();
    }

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    public boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    public List<UserDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    public UserDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    public UserDTO save(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityEmptyException {
        return service.save(user);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    public UserDTO update(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityEmptyException {
        return service.save(user);
    }

}
