package ru.t1.aayakovlev.tm.exception.entity;

public final class EntityEmptyException extends AbstractEntityException {

    public EntityEmptyException() {
        super("Error! Entity is empty...");
    }

}
