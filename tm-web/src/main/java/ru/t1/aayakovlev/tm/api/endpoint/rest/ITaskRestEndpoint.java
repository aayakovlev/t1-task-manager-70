package ru.t1.aayakovlev.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    void deleteAll() throws UserIdEmptyException;

    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    List<TaskDTO> findAll() throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAllByProjectId/{projectId}", produces = APPLICATION_JSON_VALUE)
    List<TaskDTO> findAllByProjectId(
            @PathVariable("projectId") @NotNull final String projectId
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    TaskDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @PutMapping(value = "/save", produces = APPLICATION_JSON_VALUE)
    TaskDTO save(
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException;

    @NotNull
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    TaskDTO update(
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException;

}
