package ru.t1.aayakovlev.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

@RestController
@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @GetMapping("/login")
    boolean login(@NotNull final String username, @NotNull final String password);

    @GetMapping("/logout")
    boolean logout();

    @NotNull
    @GetMapping("/profile")
    UserDTO profile();

}
