package ru.t1.aayakovlev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.rest.IProjectRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    public long count() throws AbstractException {
        return service.countByUserId(UserUtil.getUserId());
    }

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    public void deleteAll() throws AbstractException {
        projectTaskService.deleteAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        projectTaskService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    public boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAll() throws AbstractException {
        return service.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    public ProjectDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    public ProjectDTO save(
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        project.setUserId(UserUtil.getUserId());
        return service.save(project);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    public ProjectDTO update(
            @RequestBody @NotNull final ProjectDTO project
    ) throws EntityEmptyException {
        project.setUserId(UserUtil.getUserId());
        return service.save(project);
    }

}
