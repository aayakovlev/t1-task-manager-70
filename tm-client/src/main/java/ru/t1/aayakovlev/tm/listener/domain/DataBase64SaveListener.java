package ru.t1.aayakovlev.tm.listener.domain;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataBase64SaveRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-base64-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to base64 file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataBase64SaveListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE BASE64]");
        domainEndpoint.base64Save(new DataBase64SaveRequest(getToken()));
    }

}
