package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.endpoint.AuthEndpoint;
import ru.t1.aayakovlev.tm.endpoint.UserEndpoint;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected AuthEndpoint authEndpoint;

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) throws EntityNotFoundException {
        if (user == null) throw new EntityNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
    }

}
