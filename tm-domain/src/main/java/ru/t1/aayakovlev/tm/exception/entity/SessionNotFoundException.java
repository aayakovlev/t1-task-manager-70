package ru.t1.aayakovlev.tm.exception.entity;

public final class SessionNotFoundException extends AbstractEntityException {

    public SessionNotFoundException() {
        super("Error! Session not found...");
    }

}
