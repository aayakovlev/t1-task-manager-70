clean:
	mvn clean

install:
	mvn install

install-no-tests:
	mvn install -DskipTests

clean-install:
	mvn clean install

clean-install-no-tests:
	mvn clean install -DskipTests

init-scheme:
	cd tm-server && mvn liquibase:update

img:
	docker build -t tm-server .\\tm-server\\. && \
	docker build -t tm-client .\\tm-client\\. && \
	docker build -t tm-logger .\\tm-logger\\. && \
	docker build -t tm-balancer .\\tm-balancer\\.

up:
	docker-compose up -d

down:
	docker-compose down

build-img-up:
	mvn clean install && \
	docker build -t tm-server .\\tm-server\\. && \
    docker build -t tm-client .\\tm-client\\. && \
    docker build -t tm-logger .\\tm-logger\\. && \
    docker build -t tm-balancer .\\tm-balancer\\. && \
    docker-compose up -d

build-nt-img-up:
	mvn clean install -DskipTests && \
	docker build -t tm-server .\\tm-server\\. && \
    docker build -t tm-client .\\tm-client\\. && \
    docker build -t tm-logger .\\tm-logger\\. && \
    docker build -t tm-balancer .\\tm-balancer\\. && \
    docker-compose up -d

db-up:
	docker run -d \
    	--rm \
    	--name tm-db \
    	-e POSTGRES_PASSWORD=postgres \
    	-e POSTGRES_DB=task_manager \
    	-p 35432:5432 \
    	postgres:14-alpine

db-down:
	docker stop tm-db
